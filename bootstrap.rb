#!/usr/bin/ruby

puts '# Starting .file bootstrap'

#get the current working directory.

filearray = Array.new

Dir.foreach(File.dirname(__FILE__)) do |file| 
	if file =~ /^\.\w/ 
		if !File.directory?(file)
			filearray.push(file)
		end
	end
end

puts '# Creating symbolic links to repository'

filearray.each do |file|
	if File.exists?(Dir.home+"/"+file)
		puts '+ File: '+ file + ' already exists'	
	else
		File.symlink(File.absolute_path(file),Dir.home+'/'+file)	
		puts '+ File: '+ file + ' has been deployed'
	end
end

puts '#Ending .file bootstrap'
