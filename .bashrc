# Variables
export SSH_TTY=`tty | sed 's/\/dev\///g'`

#export PS1="\e[0;97m[\e[1;30m\u\e[0m\e[0;97m@\\e[1;30m\h\e[0m\e[0;97m]:\W\e[0m : "
 PS1='\e[0;36m\] \T \d \[\e[1;30m\][\[\e[1;34m\]\u@\H\[\e[1;30m\]:\[\e[0;37m\]${SSH_TTY} \[\e[0;32m\]+${SHLVL}\[\e[1;30m\]] \[\e[1;37m\]\w\[\e[0;37m\] \n($SHLVL:\!)\$ \e[0m'
export EDITOR=vim
export HISTTIMEFORMAT="%d/%m/%y %T "

# Amazon Variables
export EC2_HOME=~/.ec2
export PATH=$PATH:$EC2_HOME/bin
export EC2_PRIVATE_KEY=`ls $EC2_HOME/pk-*.pem`
export EC2_CERT=`ls $EC2_HOME/cert-*.pem`
export JAVA_HOME=/System/Library/Frameworks/JavaVM.framework/Home/

# Alias'
alias cpan='sudo perl -MCPAN -e "shell"'
alias c='builtin cd'
alias ll='ls -la'
alias l='ls'

# Pertifying
export CLICOLOR=1
export LSCOLORS=ExFxCxDxBxegedabagacad

# Startup Stuff
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

function cd() { builtin cd "$@" && ls -al; }
